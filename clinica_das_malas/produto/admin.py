from django.contrib import admin
from .models import Produto
# Register your models here.

from .models import Produto


class ProdutoAdmin(admin.ModelAdmin):

	list_display = ['nome', 'quantidade', 'qtd_min']
	search_fields = ['nome']


admin.site.register(Produto, ProdutoAdmin)