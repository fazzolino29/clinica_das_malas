"""clinica_das_malas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from clinica_das_malas.core.views import home

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^conta/', include('clinica_das_malas.accounts.urls', namespace="accounts")),
    url(r'^', include('clinica_das_malas.core.urls', namespace="core")),
    url(r'^bagagens/', include('clinica_das_malas.bagagem.urls', namespace="bagagem")),
    url(r'^estoque/', include('clinica_das_malas.produto.urls', namespace="produto")),
    url(r'^index/', include('clinica_das_malas.index.urls', namespace="index")),
    url(r'^tecnico/', include('clinica_das_malas.tecnico.urls', namespace="tecnico"))
]
