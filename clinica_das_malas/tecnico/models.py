from django.db import models

# Create your models here.
class Tecnico(models.Model):
    nome = models.CharField(max_length=40)
    email = models.EmailField('E-mail', unique=True)
    telefone = models.CharField(max_length=30)
    endereco = models.CharField(max_length=50)

    comissao = models.FloatField()
    salario = models.FloatField(default=0)

    is_active = models.BooleanField('Ativo', default=True)
    date_joined = models.DateTimeField('Data de Entrada', auto_now_add=True)

    def __str__(self):
        return self.nome