import json
from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from clinica_das_malas.bagagem.models import Bagagem
from clinica_das_malas.tecnico.models import Tecnico
from clinica_das_malas.tipoServico.models import TipoServico


def historico(request, pk):
    bagagens = Bagagem.objects.all().filter(tecnico=pk)
    tecnico = Tecnico.objects.get(pk=pk)
    context = {
        'bagagens':bagagens,
        'tecnico': tecnico
    }
    return render(request, 'historico.html', context)


def pesquisar(request):
    tecnicos = Tecnico.objects.all()

    context = {
        'tecnicos': tecnicos
    }
    return render(request, 'pesquisa.html', context)


def geral(request):

    return render(request, 'geral.html')