from django.conf.urls import url
from .views import list_bagagens, show_bagagem, entra_bagagem, cliente_bagagens

urlpatterns = [
    url(r'^$', list_bagagens, name="list"),
    url(r'^(\d+)', show_bagagem, name="show"),
    url(r'^entra/$', entra_bagagem, name="entra_bagagem"),
    url(r'^cliente/$', cliente_bagagens, name="cliente"),
]
